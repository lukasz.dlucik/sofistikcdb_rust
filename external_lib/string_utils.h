﻿/* ------------------------------------------------------------------------ */
/* String Utility routines for cdbase                                       */
/* Copyright SOFiSTiK GmbH, 85764 Oberschleissheim                          */
/* ------------------------------------------------------------------------ */

#pragma once

#include <string>

#if !defined(SOF_C_DLL_API) 
#  if defined(_WIN32)
#     define SOF_C_DLL_API __declspec(dllimport)
#  else
#     define SOF_C_DLL_API
#  endif
#endif

typedef unsigned int pckcode;       /* Gepackter Unicode 32 Bit */

/* extract Pckstring */
SOF_C_DLL_API std::wstring sof_lib_ps2wstring(const pckcode* p_str);
/*pack  wstring into p_str[mlen]    */
SOF_C_DLL_API void sof_lib_wstring2ps(std::wstring &text, pckcode* p_str, size_t mlen);

SOF_C_DLL_API void sof_lib_cs2ps(const char* c_str, pckcode* e_str, int e_str_mlen);
SOF_C_DLL_API void sof_lib_ps2cs(pckcode *pc, char *s, int len);

SOF_C_DLL_API void    sof_lib_4cpack(int* p, const char* c);      /* 4 Char in int     */
SOF_C_DLL_API void    sof_lib_4cunpack(const int* p, char* c);    /* 4 Char aus int    */
SOF_C_DLL_API std::string sof_lib_cunpack(const int* pack, int packlen);
