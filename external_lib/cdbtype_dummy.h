/*   SOFiSTiK AG
     cdbtype_dummy.h
     automatically generated header, do not modify
*/


#pragma once

#ifndef SOF_CHARS_IN_PACKCODE
#define SOF_CHARS_IN_PACKCODE(pack) ((sizeof(pack)/sizeof(pack[0]))*2 - 1) 
#endif

typedef int bhr;
typedef int chr;
typedef unsigned int pckcode;

#ifndef SOF_CDBASE_VERSION
#define SOF_CDBASE_VERSION 264321
#endif


/*   SOFiSTiK AG
     cdbtype_dummy.h
     end of automatically generated header
*/
