/*   SOFiSTiK AG
     cdbtypeall.h
     automatically generated header, do not modify
*/


#pragma once

#ifndef SOF_CHARS_IN_PACKCODE
#define SOF_CHARS_IN_PACKCODE(pack) ((sizeof(pack)/sizeof(pack[0]))*2 - 1) 
#endif

typedef int bhr;
typedef int chr;
typedef unsigned int pckcode;

#ifndef SOF_CDBASE_VERSION
#define SOF_CDBASE_VERSION 264321
#endif

#include "cdbtypemat.h"
#include "cdbtypesct.h"
#include "cdbtypegeo.h"
#include "cdbtypeten.h"
#include "cdbtypelfc.h"
#include "cdbtypesys.h"

/*   SOFiSTiK AG
     cdbtypeall.h
     end of automatically generated header
*/
