/*   SOFiSTiK AG
     cdbtypesct.h
     automatically generated header, do not modify
*/


#pragma once

#ifndef SOF_CHARS_IN_PACKCODE
#define SOF_CHARS_IN_PACKCODE(pack) ((sizeof(pack)/sizeof(pack[0]))*2 - 1) 
#endif

typedef int bhr;
typedef int chr;
typedef unsigned int pckcode;

#ifndef SOF_CDBASE_VERSION
#define SOF_CDBASE_VERSION 264321
#endif

#define MAXQNR   9999

#define SECT_ID 0
#define SECT_VER 200501
typedef struct tagCDB_SECT {     /* 9/NR  SectionalValues (total section) */
int     m_id;                    /*        Identification */
int     m_mno;                   /*        Materialnumber of sections */
int     m_mrf;                   /*        Materialnumber of Reinforcement */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of elasticity */
float   m_zs;                    /* [1011] z-Ordinate Center of elasticity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus */
float   m_gm;                    /* [1090] Shear Modulus */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT;

#define SECT_EFF_ID 1
#define SECT_EFF_VER 200501
typedef struct tagCDB_SECT_EFF { /* 9/NR  SectionalValues (effective section) */
int     m_id;                    /*        Identification */
int     m_mno;                   /*        Materialnumber of sections */
int     m_mrf;                   /*        Materialnumber of Reinforcement */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of elasticity */
float   m_zs;                    /* [1011] z-Ordinate Center of elasticity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus */
float   m_gm;                    /* [1090] Shear Modulus */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT_EFF;

#define SECT_PAR_ID 2
#define SECT_PAR_VER 200501
typedef struct tagCDB_SECT_PAR { /* 9/NR  SectionalValues (total part of section) */
int     m_id;                    /*        Identification */
int     m_mno;                   /*        Materialnumber of sections */
int     m_mrf;                   /*        Materialnumber of Reinforcement */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of elasticity */
float   m_zs;                    /* [1011] z-Ordinate Center of elasticity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus */
float   m_gm;                    /* [1090] Shear Modulus */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT_PAR;

#define SECT_PEF_ID 3
#define SECT_PEF_VER 200501
typedef struct tagCDB_SECT_PEF { /* 9/NR  SectionalValues (effective part of section) */
int     m_id;                    /*        Identification */
int     m_mno;                   /*        Materialnumber of sections */
int     m_mrf;                   /*        Materialnumber of Reinforcement */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of elasticity */
float   m_zs;                    /* [1011] z-Ordinate Center of elasticity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus */
float   m_gm;                    /* [1090] Shear Modulus */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT_PEF;

#define SECT_ERR_VER 200501
typedef struct tagCDB_SECT_ERR { /* 9/NR  Invalid section, visualisation only */
int     m_id;                    /*        Identification */
int     m_mno;                   /*        Materialnumber of sections */
int     m_mrf;                   /*        Materialnumber of Reinforcement */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of elasticity */
float   m_zs;                    /* [1011] z-Ordinate Center of elasticity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus */
float   m_gm;                    /* [1090] Shear Modulus */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT_ERR;

#define SECT_ADD_ID 4
#define SECT_ADD_VER 200501
typedef struct tagCDB_SECT_ADD { /* 9/NR  SectionalValuesShear , Temperature */
int     m_id;
int     m_stype;
#define SECT_ADD_MODONLY -1
#define SECT_ADD_MODIFIED 0
#define SECT_ADD_VALUES 1
#define SECT_ADD_STANDARD 2
#define SECT_ADD_PANEL 3
#define SECT_ADD_SOLID 4
#define SECT_ADD_EFF 5
#define SECT_ADD_FEM 6
#define SECT_ADD_HYDRAULIC 9
#define SECT_ADD_FIBRE 128
#define SECT_ADD_ELEMENTS 256
#define SECT_ADD_NEFF 512
#define SECT_ADD_INTE 1024
#define SECT_ADD_TEMPLATE 2048
#define SECT_ADD_VARIABLE 4096
#define SECT_ADD_REFP 8192
#define SECT_ADD_HOT 16384
#define SECT_ADD_RF_GROSS 32768
#define SECT_ADD_RF_GAM 65536
#define SECT_ADD_RF_Eff 131072
#define SECT_ADD_RF_UNUSED 262144
int     m_mrf;                   /*        Materialnumber of Stirup-Reinforcement */
float   m_at;                    /* [ 107] Elongationcoefficient for Temperature */
float   m_ymin;                  /* [1011] Minimum Ordinate of section to center ys */
float   m_ymax;                  /* [1011] Maximum Ordinate of section to center ys */
float   m_zmin;                  /* [1011] Minimum Ordinate of section to center zs */
float   m_zmax;                  /* [1011] Maximum Ordinate of section to center zs */
float   m_tmin;                  /* [1011] minimum thickness of plates */
float   m_tmax;                  /* [1011] maximum thickness of plates */
float   m_wt;                    /* [1018] maximum tau for primary Torsion Mtp=1 */
float   m_wvy;                   /* [1017] maximum tau for Shear Vy=1 */
float   m_wvz;                   /* [1017] maximum tau for Shear Vz=1 */
float   m_wt2;                   /* [1018] maximum tau for secondary Torsion Mt2=1 */
float   m_ak;                    /* [1012] kernel area for Torsion (Bredt) */
float   m_ayz;                   /* [1012] Shear deviation area */
float   m_ab;                    /* [1012] pure concrete area */
float   m_levy;                  /* [1011] minimum lever for cracked shear Vy */
float   m_levz;                  /* [1011] minimum lever for cracked shear Vz */
float   m_elvy;                  /* [  17] elastic shear flux for Vy = Sy-max/Iz */
float   m_elvz;                  /* [  17] elastic shear flux for Vz = Sz-max/Iy */
float   m_ymine;                 /* [1011] Minimum Ordinate of effective section */
float   m_ymaxe;                 /* [1011] Maximum Ordinate of effective section */
float   m_zmine;                 /* [1011] Minimum Ordinate of effective section */
float   m_zmaxe;                 /* [1011] Maximum Ordinate of effective section */
} typeCDB_SECT_ADD;

#define SECT_WAR_ID 5
#define SECT_WAR_VER 200501
typedef struct tagCDB_SECT_WAR { /* 9/NR  SectionalValuesWarping */
int     m_id;                    /*        Identification = 5 */
int     m_i_1;                   /*        unused */
int     m_i_2;                   /*        unused */
float   m_ayyy;                  /* [1015] Integrals on section */
float   m_ayyz;                  /* [1015] Integrals on section */
float   m_ayzz;                  /* [1015] Integrals on section */
float   m_azzz;                  /* [1015] Integrals on section */
float   m_awy;                   /* [1015] Integrals on section */
float   m_awz;                   /* [1015] Integrals on section */
float   m_awyy;                  /* [1016] Integrals on section */
float   m_awzz;                  /* [1016] Integrals on section */
float   m_cm;                    /* [1016] Warping resistance */
float   m_cms;                   /* [1014] Warping Shear resist. */
float   m_wmin;                  /* [1012] Minimum warping */
float   m_wmax;                  /* [1012] Maximum warping */
float   m_fysw;                  /* [1012] Shear VY-MT2 area */
float   m_fzsw;                  /* [1012] Shear VZ-MT2 area */
float   m_ipsc;                  /* [1014] polar inertia at SC */
} typeCDB_SECT_WAR;

#define SECT_PLA_ID 6
#define SECT_PLA_VER 200501
typedef struct tagCDB_SECT_PLA { /* 9/NR  SectionalPlasticForces */
int     m_id;                    /*        Identification = 6 */
int     m_ibc;                   /*        Buckling curve */
#define SECT_IBC_UNCHANGED -1
#define SECT_IBC_NONE 0
#define SECT_IBC_A 1
#define SECT_IBC_B 2
#define SECT_IBC_C 3
#define SECT_IBC_D 4
#define SECT_IBC_E 5
#define SECT_IBC_A0 8
#define SECT_IBC_OFF 30
#define SECT_IBC_DEFAULT 31
#define SECT_IBC_MULT 32
int     m_ib;                    /*        Control */
#define SECT_PLA_KIND_ELASTIC 0
#define SECT_PLA_KIND_PLASTIC 1
#define SECT_PLA_KIND_COMBINATION 2
#define SECT_PLA_KIND_COMB_NONSYMM 3
#define SECT_PLA_VAL_DESIGN 0
#define SECT_PLA_VAL_CHARACTERISTIC 1
#define SECT_PLA_VAL_MULT 100
#define SECT_PLA_INFO_SYMMETRIC 1
#define SECT_PLA_INFO_BUCKLING 2
#define SECT_PLA_INFO_GAMMA_M0 4
#define SECT_PLA_INFO_GAMMA_SCM 8
#define SECT_PLA_INFO_COMPOSITE 16
#define SECT_PLA_INFO_HOLLOW 32
#define SECT_PLA_INFO_MULT 10000
float   m_wpn;                   /* [1101] Normal force */
float   m_wpvy;                  /* [1102] Y-Shear force */
float   m_wpvz;                  /* [1102] Y-Shear force */
float   m_wpmt;                  /* [1103] Torsional moment */
float   m_wpmy;                  /* [1104] Bending moment My */
float   m_wpmz;                  /* [1104] Bending moment Mz */
float   m_ysp;                   /* [1001] Plastic center */
float   m_zsp;                   /* [1001] Plastic center */
float   m_wpmb;                  /* [1105] warping moment Mb */
float   m_wpt2;                  /* [1103] 2nd torsional moment */
} typeCDB_SECT_PLA;

#define SECT_DES_ID 7
#define SECT_DES_VER 200501
typedef struct tagCDB_SECT_DES { /* 9/NR  SectionalValuesDesign */
int     m_id;                    /*        Identification = 7 */
int     m_mnr;                   /*        used reference material */
int     m_ibc;                   /*        buckling curve index (see 009/NR:6) */
float   m_a;                     /* [1012] Area */
float   m_ay;                    /* [1012] Sheardeformation area Y */
float   m_az;                    /* [1012] Sheardeformation area Z */
float   m_it;                    /* [1014] Torsional moment of inertia */
float   m_iy;                    /* [1014] Moments of inertia y-y */
float   m_iz;                    /* [1014] Moments of inertia z-z */
float   m_iyz;                   /* [1014] Moments of inertia y-z */
float   m_ys;                    /* [1011] y-Ordinate Center of gravity */
float   m_zs;                    /* [1011] z-Ordinate Center of gravity */
float   m_ysc;                   /* [1011] y-Ordinate of Shear-Center */
float   m_zsc;                   /* [1011] z-Ordinate of Shear-Center */
float   m_em;                    /* [1090] Elasticity Modulus/safety */
float   m_gm;                    /* [1090] Shear Modulus/safety */
float   m_gam;                   /* [1091] Nominal weight */
} typeCDB_SECT_DES;

#define SECT_PRE_ID 8
#define SECT_PRE_VER 200501
typedef struct tagCDB_SECT_PRE { /* 9/NR  SectionalForcesPrestress */
int     m_id;                    /*        Identification = 8 */
int     m_itp;                   /*        subtype of information = 0 */
int     m_its;                   /*        source of values */
float   m_n;                     /* [1101] Normal force */
float   m_my;                    /* [1104] Bending moment My */
float   m_mz;                    /* [1104] Bending moment Mz */
float   m_ea;                    /* [1101] tangential stiffnes E*A */
float   m_eay;                   /* [1105] tangential stiffnes E*A*z*z */
float   m_eaz;                   /* [1105] tangential stiffnes E*A*y*y */
float   m_eayz;                  /* [1105] tangential stiffnes E*A*y*z */
float   m_yse;                   /* [1011] elastic tangential center ys */
float   m_zse;                   /* [1011] elastic tangential center zs */
float   m_ncm;                   /* [1101] maximum compressive force */
float   m_ntm;                   /* [1101] maximum tensile force */
} typeCDB_SECT_PRE;

#define SECT_GV_ID 8
#define SECT_GV_VER 200501
typedef struct tagCDB_SECT_GV {  /* 9/NR  SectionalGeometricValues */
int     m_id;                    /*        Identification = 8 */
int     m_itp;                   /*        subtype of information = 2 */
int     m_mno;                   /*        Materialnumber (0=total section) */
float   m_uo;                    /* [1011] outer perimeter */
float   m_ui;                    /* [1011] inner perimeter */
float   m_tmin;                  /* [1011] minimum thickness */
float   m_tmax;                  /* [1011] maximum thickness */
float   m_ktz;                   /* [   8] total weight factor */
float   m_uom;                   /* [1011] outer effect.perimeter */
float   m_uim;                   /* [1011] inner effect.perimeter */
float   m_aro;                   /* [1012] Outer total gross area */
float   m_ar;                    /* [1012] total gross area */
float   m_ipm;                   /* [1183] rotational inertia      gam*Ip  ! values to be converted * DC_M */
float   m_iym;                   /* [1183] sectional mass inertia  gam*Iy  ! values to be converted * DC_M */
float   m_izm;                   /* [1183] sectional mass inertia  gam*Iz  ! values to be converted * DC_M */
float   m_iyzm;                  /* [1183] sectional mass inertia  gam*Iyz ! values to be converted * DC_M */
float   m_ysm;                   /* [1011] true mass gravity center */
float   m_zsm;                   /* [1011] true mass gravity center */
float   m_dw;                    /* [1011] effective thickness for creep/shrinkage */
float   m_uair;                  /* [1011] air contact perimeter */
} typeCDB_SECT_GV;

#define SECT_TRA_ID 8
#define SECT_TRA_VER 201306
typedef struct tagCDB_SECT_TRA { /* 9/NR  SectionalAppliedTransformation */
int     m_id;                    /*        Identification = 8 */
int     m_itp;                   /*        subtype of information = 3 */
int     m_i_2;                   /*        not used */
float   m_alf;                   /* [   5] rotation angle */
float   m_ytf;                   /* [1011] shift in y direction */
float   m_ztf;                   /* [1011] shift in z direction */
} typeCDB_SECT_TRA;

#define SECT_LAY_ID 9
#define SECT_LAY_VER 201705
typedef struct tagCDB_SECT_LAY { /* 9/NR  SectionalReinforcementLayer  (LAY) */
int     m_id;                    /*        Identification = 9 */
int     m_mnr;                   /*        Materialnumber + 1000*No of concrete */
#define LAY_MNR_REF 1000
int     m_inr;                   /*        Type/number of layer */
#define LAY_INR_TYPE 100
#define LAY_INR_TYPE_MINIMUM 1
#define LAY_INR_TYPE_ADDITIONAL 2
#define LAY_INR_TYPE_SEQUENTIAL 3
chr     m_tnr;                   /*        Designation */
float   m_alow;                  /* [1020] Lower bound for gross area of layer */
float   m_ahig;                  /* [1020] Upper bound for gross area of layer */
float   m_asum;                  /* [1020] Sum of reference gross areas of layer */
float   m_afak;                  /*        maximum allowable factor of layer */
float   m_ys;                    /* [1011] center of layer (gross) */
float   m_zs;                    /* [1011] center of layer (gross) */
float   m_vas;                   /* [1020] Lower sequential total bound of layer area */
float   m_bas;                   /* [1020] Upper sequential total bound of layer area */
float   m_iy;                    /* [1014] 4th order areas (gross inertia) */
float   m_iz;                    /* [1014] 4th order areas (gross inertia) */
float   m_iyz;                   /* [1014] 4th order areas (gross inertia) */
float   m_emod;                  /* [1090] Elasticity-Modulus */
float   m_bets;                  /* [1092] yieldstress */
float   m_rl;                    /* [1011] torsional refer. length */
float   m_dmin;                  /* [1023] Smallest used diameter */
float   m_dmax;                  /* [1023] Largest  used diameter */
float   m_n;                     /* [1101] Prestress normal force N */
float   m_my;                    /* [1104] Prestress bending moment My */
float   m_mz;                    /* [1104] Prestress bending moment Mz */
float   m_tn;                    /* [1101] Effective Normal force N    from temperature eigenstress */
float   m_tmy;                   /* [1104] Effective Bending moment My from temperature eigenstress */
float   m_tmz;                   /* [1104] Effective Bending moment Mz from temperature eigenstress */
float   m_ea;                    /* [1101] Effective tangential stiffnes E*A */
float   m_eay;                   /* [1104] Effective tangential stiffnes E*A*y */
float   m_eaz;                   /* [1104] Effective tangential stiffnes E*A*z */
float   m_eayy;                  /* [1105] Effective tangential stiffnes E*A*y*y */
float   m_eazz;                  /* [1105] Effective tangential stiffnes E*A*z*z */
float   m_eayz;                  /* [1105] Effective tangential stiffnes E*A*y*z */
float   m_nplc;                  /* [1101] Effective maximum compressive force */
float   m_nplt;                  /* [1101] Effective maximum tensile force */
} typeCDB_SECT_LAY;

#define SECT_REC_ID 10
#define SECT_REC_VER 201302
typedef struct tagCDB_SECT_REC { /* 9/NR  SectiontypeRectangular T-Beam (SREC) */
int     m_id;                    /*        Identification = 10 */
int     m_iq;                    /*        Identification of section */
#define SECT_REC_PLATE 0
#define SECT_REC_RECT 1
#define SECT_REC_TBEAM 2
#define SECT_REC_TTBEAM 3
#define SECT_REC_VPLAT 4
#define SECT_REC_VHREC 5
#define SECT_REC_VBREC 6
#define SECT_REC_VRECT 7
#define SECT_REC_REFPT 256
#define SECT_REC_SCLOC 8192
#define SECT_REC_IBCOFF 32768
int     m_ir;                    /*        Identification of reinforcement and origin */
#define SECT_REC_RF_CU 0
#define SECT_REC_RF_SYM 1
#define SECT_REC_RF_ASY 2
#define SECT_REC_RF_AST 3
#define SECT_REC_RF_ASB 4
#define SECT_REC_CORNER 8
#define SECT_REC_RF_TRA 64
#define SECT_REC_BUNDLE 128
#define SECT_REC_FIXLAY 256
float   m_h;                     /* [1011] total height */
float   m_b;                     /* [1011] width (of web) */
float   m_so;                    /* [1024] Boundary distance of upper reinforcement */
float   m_su;                    /* [1024] Boundary distance of lower reinforcement */
float   m_aso;                   /* [1020] reinforcement up */
float   m_asu;                   /* [1020] reinforcement down */
float   m_ho;                    /* [1011] height of plate */
float   m_bo;                    /* [1011] width  of plate */
float   m_daso;                  /* [1023] reinf. Diameter up */
float   m_dasu;                  /* [1023] reinf. Diameter dn */
float   m_fkit;                  /*        <0 = factor for torsional inertia >0 = absolut value of torsion */
float   m_fkay;                  /*        <0 = factor for shear deformation area >0 = absolut value of sh */
float   m_fkaz;                  /*        <0 = factor for shear deformation area >0 = absolut value of sh */
float   m_ysmp;                  /* [1011] explicit location of shear center */
float   m_zssmp;                 /* [1011] explicit location of shear center */
float   m_ys;                    /* [1011] explicit location of reference point */
float   m_zs;                    /* [1011] explicit location of reference point */
float   m_phib;                  /* [   5] inclination of transverse reinforcements */
float   m_beff;                  /* [1011] width of equivalent hollow section */
float   m_asbm;                  /*        Minimum shear reinf. */
float   m_a;                     /* [1011] spacing of reinforcements */
float   m_amin;                  /* [1011] minimum distance of single reinforcements */
float   m_amax;                  /* [1011] maximum distance of single reinforcements */
float   m_ss;                    /* [1024] Boundary distance of lateral reinforcement */
float   m_dass;                  /* [1023] reinf. Diameter side */
float   m_hmax;                  /* [1011] maximum mesh size for FE/fibre analysis */
float   m_betc;                  /*        friction coefficient */
float   m_mue;                   /*        friction in joint */
} typeCDB_SECT_REC;

#define SECT_ANN_ID 11
#define SECT_ANN_VER 201212
typedef struct tagCDB_SECT_ANN { /* 9/NR  SectiontypeAnnular (SCIT) */
int     m_id;                    /*        Identification = 11 */
int     m_ir;                    /*        Identification of section */
int     m_ir2;                   /*        Identification of reinforcement */
float   m_ro;                    /* [1011] outer radius */
float   m_ri;                    /* [1011] inner radius */
float   m_ros;                   /* [1011] outer reinf radius */
float   m_ris;                   /* [1011] inner reinf radius */
float   m_aso;                   /* [1020] outer reinforcement */
float   m_asi;                   /* [1020] inner reinforcement */
float   m_daso;                  /* [1023] reinf. Diameter */
float   m_amax;                  /* [1011] maximum distance of single reinforcements */
float   m_asbm;                  /* [1021] Minimum shear reinf. */
float   m_hmax;                  /* [1011] maximum mesh size for FE/fibre analysis */
float   m_fkit;                  /*        <0 = factor for torsional inertia >0 = absolut value of torsion */
float   m_fkay;                  /*        <0 = factor for shear deformation area >0 = absolut value of sh */
float   m_fkaz;                  /*        <0 = factor for shear deformation area >0 = absolut value of sh */
} typeCDB_SECT_ANN;

#define SECT_TUB_ID 11
#define SECT_TUB_VER 200501
typedef struct tagCDB_SECT_TUB { /* 9/NR  SectiontypeTube (TUBE) */
int     m_id;                    /*        Identification = 11 */
int     m_ir;                    /*        Identification of section = 2 */
int     m_ir2;                   /*        property Bit */
float   m_d;                     /* [1011] Diameter */
float   m_t;                     /* [1011] thickness of tubes */
} typeCDB_SECT_TUB;

#define SECT_CAB_ID 11
#define SECT_CAB_VER 200501
typedef struct tagCDB_SECT_CAB { /* 9/NR  SectiontypeCable (CABL) */
int     m_id;                    /*        Identification = 11 */
int     m_ir;                    /*        Identification of section = 3 */
int     m_ic;                    /*        sectiontype*256 + codeflag*64 + inlet type */
float   m_d;                     /* [1011] Diameter */
chr     m_typ[2];                /*        Literal of type */
chr     m_inl;                   /*        Literal of inlet */
float   m_nli;                   /*        no of strands */
float   m_ndr;                   /*        no of wires */
float   m_f;                     /*        fill factor */
float   m_k;                     /*        cable factor */
float   m_w;                     /*        weight factor */
float   m_ke;                    /*        loss factor */
float   m_zd;                    /* [1090] design  force */
float   m_zb;                    /* [1090] rupture force */
float   m_gam;                   /* [1188] weight */
} typeCDB_SECT_CAB;

#define SECT_PRO_ID 12
#define SECT_PRO_VER 201311
typedef struct tagCDB_SECT_PRO { /* 9/NR  SectiontypeShape (PROF) */
int     m_id;                    /*        Identification = 12 */
int     m_mno;                   /*        Materialnumber + 1000*imbedded Materialnumber */
int     m_ibit;                  /*        ityp + SECT_PROF_PROP_MULT*(iref+ivtp) */
#define PROF_PARALLEL_DOUBLE_T 0
#define PROF_INCLINED_DOUBLE_T 1
#define PROF_PARALLEL_U_C 2
#define PROF_INCLINED_U_C 3
#define PROF_PARALLEL_T 4
#define PROF_INCLINED_T 5
#define PROF_HOT_FORMED_L 6
#define PROF_COLD_FORMED_L 7
#define PROF_RECTANGULAR_HOLLOW_SHAPE 8
#define PROF_Z_SHAPE 9
#define PROF_SHEET_PILE_U_TYPE 10
#define PROF_SHEET_PILE_Z_TYPE 11
#define PROF_COLD_FORMED_U_C_Z 12
#define PROF_RECTANGULAR_BAR 15
#define PROF_BAR_TUBE_PIPE 16
#define PROF_REF_STD 0
#define PROF_REF_S 1
#define PROF_REF_SM 2
#define PROF_REF_OR 3
#define PROF_REF_OL 4
#define PROF_REF_OM 5
#define PROF_REF_R 6
#define PROF_REF_L 7
#define PROF_REF_M 8
#define PROF_REF_UR 9
#define PROF_REF_UL 10
#define PROF_REF_UM 11
#define PROF_PROP_SOLID 16
#define PROF_PROP_HALF 32
#define PROF_PROP_MIRROR_ORIGIN 64
#define PROF_PROP_MIRROR_Y_AXIS 128
#define PROF_PROP_MIRROR_Z_AXIS 256
#define PROF_PROP_FLIP_TO_Y_AXIS 192
#define PROF_PROP_FLIP_TO_Z_AXIS 320
#define PROF_PROP_MIRROR_4_ITEMS 448
#define PROF_PROP_CUSTOM_SHAPE 512
#define PROF_PROP_CALCULATION 1024
#define PROF_PROP_SIMPLIFIED_WIND 2048
#define PROF_PROP_IMPERIAL_UNITS 4096
#define PROF_PROP_SELECTOR_METRIC 8192
#define PROF_PROP_R_AS_K_FOR_HAUNCH 16384
#define PROF_PROP_RECORD_PROF 32768
#define PROF_PROP_ERRORFLAG 65536
#define SECT_PROF_PROP_MULT 64
chr     m_typ;                   /*        type of section */
float   m_z1;                    /*        nominal first size */
float   m_z2;                    /*        nominal second size */
float   m_z3;                    /*        thickness or weight selector */
float   m_alf;                   /* [   5] rotation angle */
float   m_it;                    /* [1014] tabul.Tors.Inertia */
float   m_ysc;                   /* [1011] tabul.shear center */
float   m_zsc;                   /* [1011] tabul.shear center */
float   m_h;                     /* [1011] height */
float   m_b;                     /* [1011] width */
float   m_s;                     /* [1011] thickness of web */
float   m_t;                     /* [1011] thickness of flange */
float   m_r1;                    /* [1011] radius */
float   m_r2;                    /* [1011] radius */
float   m_yoff;                  /* [1011] y coord. offset */
float   m_zoff;                  /* [1011] z coord. offset */
float   m_cm;                    /* [1016] tabul.warp.resist. */
float   m_wmin;                  /* [1012] charact. warping 1 */
float   m_wmax;                  /* [1012] charact. warping 2 */
float   m_wt;                    /* [1018] Torsional resist. */
float   m_ay;                    /* [1012] Shear area */
float   m_az;                    /* [1012] Shear area */
chr     m_rid;                   /*        user defined ident */
float   m_wu1;                   /* [1011] bolt position 1 */
float   m_wu2;                   /* [1011] bolt position 2 */
float   m_wu3;                   /* [1011] bolt position 3 */
float   m_yref;                  /* [1011] Coordinates of */
float   m_zref;                  /* [1011] Reference point */
float   m_b2;                    /* [1011] width lower part */
float   m_t2;                    /* [1011] lower flange thick. */
chr     m_bez[8];                /*        character of shape */
float   m_vypl;                  /* [1102] characteristic plastic shear force Vy */
float   m_vzpl;                  /* [1102] characteristic plastic shear force Vz */
float   m_mtpl;                  /* [1103] characteristic plastic torsional   Mt */
float   m_mt2pl;                 /* [1103] characteristic plastic torsional   Mt2 */
float   m_mbpl;                  /* [1103] characteristic plastic warpmoment  Mb */
} typeCDB_SECT_PRO;

#define SECT_TBA_ID 18
#define SECT_TBA_VER 201509
typedef struct tagCDB_SECT_TBA { /* 9/NR  List of construction stages */
int     m_id;                    /*        Identification = 18 */
int     m_iba;                   /*        Headder of construction stages = 0 */
int     m_ibd;                   /*        Number of materials over all construction stages */
int     m_ibat[64];              /*        List of external construction stage numbers */
} typeCDB_SECT_TBA;

#define SECT_IBA_ID 18
#define SECT_IBA_VER 200501
typedef struct tagCDB_SECT_IBA { /* 9/NR  ConstructionStage (CS/BA) */
int     m_id;                    /*        Identification = 18 */
int     m_iba;                   /*        number of construction stage BIRTH */
int     m_ibd;                   /*        number of construction stage DEATH */
pckcode m_txt[17];               /*        Text */
#define SECT_IBA_TXT_LEN 32
} typeCDB_SECT_IBA;

#define SECT_TXT_ID 19
#define SECT_TXT_VER 200501
typedef struct tagCDB_SECT_TXT { /* 9/NR  SectionText */
int     m_id;                    /*        Identification = 19 */
int     m_nr;                    /*        external construction stage number for CS separator */
int     m_ids;                   /*        Source/Selector flag */
#define TXT_IDS_GENERATED_TEXT 0
#define TXT_IDS_USER_TEXT 1
#define TXT_IDS_INTERNAL_CDB 90
#define TXT_IDS_EXTERNAL_CDB 91
#define TXT_IDS_CS_BIRTH 99
#define TXT_IDS_TEMPLATE 100
pckcode m_txt[128];              /*        Text */
#define SECT_TXT_TXT_LEN 255
} typeCDB_SECT_TXT;

#define SECT_WLS_VER 200501
typedef struct tagCDB_SECT_WLS { /* 9/NR  SectionWorklaw Serviceability Analysis */
int     m_id;                    /*        Identification */
int     m_n;                     /*        Normal force 0/00 */
int     m_ntyp;                  /*        Type of law */
float   m_arb[20][5];            /*        Function values */
} typeCDB_SECT_WLS;

#define SECT_WLU_VER 200501
typedef struct tagCDB_SECT_WLU { /* 9/NR  SectionWorklaw Ultimate Analysis */
int     m_id;                    /*        Identification */
int     m_n;                     /*        Normal force 0/00 */
int     m_ntyp;                  /*        Type of law */
float   m_arb[20][5];            /*        Function values */
} typeCDB_SECT_WLU;

#define SECT_CW_ID 80
#define SECT_CW_VER 201301
typedef struct tagCDB_SECT_CW {  /* 9/NR  Hydrodynamic coefficients (WIND/Wave loading) */
int     m_id;                    /*        Identification 80 */
int     m_iref;                  /*        Reference of coefficients */
int     m_alf;                   /*        angle of attack in degrees */
float   m_cy;                    /*        wind reaction coefficient in y-direction */
float   m_cz;                    /*        wind reaction coefficient in z-direction */
float   m_ct;                    /*        wind reaction coefficient for torsional moment */
float   m_chyd;                  /*        hydrodynamic coefficient (Morrison equation) */
float   m_clat;                  /*        lateral wind reaction coefficient */
float   m_s;                     /*        Strouhal number */
float   m_ag;                    /*        reserved (must be present to detect old data without chyd) */
} typeCDB_SECT_CW;

#define SECT_WPA_ID 81
#define SECT_WPA_VER 200501
typedef struct tagCDB_SECT_WPA { /* 9/NR  SectionWindParameters for Wind loading (WPAR) */
int     m_id;                    /*        Identification 81 */
int     m_iba;                   /*        construction stage number */
int     m_ibit;                  /*        bitpattern of explicit limits */
float   m_kd;                    /* [1011] equivalent roughness k */
float   m_ice;                   /* [1011] ice cover */
float   m_traf;                  /* [1011] height of traffic lane */
float   m_ymin;                  /* [1011] wind attack area */
float   m_zmin;                  /* [1011] wind attack area */
float   m_ymax;                  /* [1011] wind attack area */
float   m_zmax;                  /* [1011] wind attack area */
float   m_yref;                  /* [1011] wind moment reference */
float   m_zref;                  /* [1011] wind moment reference */
float   m_corn;                  /* [1011] radius of round edge for rectangular shapes */
float   m_cwf;                   /*        explicit factor or value of wind coefficient */
float   m_alf;                   /* [   5] rotation for default wind coefficients */
} typeCDB_SECT_WPA;

#define SECT_WDE_ID 82
#define SECT_WDE_VER 200601
typedef struct tagCDB_SECT_WDE { /* 9/NR  SectionWindDerivativa for Wind loading (WIND) */
int     m_id;                    /*        Identification 82 */
int     m_iref;                  /*        Type of derivativa */
int     m_alf;                   /*        angle of attack in degrees (see 009/NR:80) */
float   m_der[20][5];            /*        Derivativa vred,value as splines */
} typeCDB_SECT_WDE;

#define SECT_USR_ID 90
#define SECT_USR_VER 200502
typedef struct tagCDB_SECT_USR { /* 9/NR  SectionUserValues (SV/QW) */
int     m_id;                    /*        Identification = 90 */
int     m_itp;                   /*        type of control value set */
#define USR_RVAL_PROG_ABS_COOR 0
#define USR_RVAL_PROG_REF_COOR 1
#define USR_ITP_UNIT_OUT 0
#define USR_ITP_MNR 1
#define USR_ITP_MBW 2
#define USR_ITP_MBR 3
#define USR_ITP_BEAM_TYPE 4
#define USR_IVAL_IBTYP_CENTRIC_BEAM 0
#define USR_IVAL_IBTYP_ECCENTRIC_BEAM 1
#define USR_IVAL_IBTYP_TRUSS 2
#define USR_IVAL_IBTYP_CABLE 3
#define USR_IVAL_IBTYP_CENTRIC_COLUMN 4
#define USR_IVAL_IBTYP_ECCENTRIC_COLUMN 5
#define USR_IVAL_IBTYP_PIPE 9
#define USR_ITP_NRQMASTER 6
#define USR_ITP_FACE 7
#define USR_IVAL_FACE_HORIZONTAL_DEFAULT 1
#define USR_IVAL_FACE_DOWN 2
#define USR_IVAL_FACE_HORIZONTAL_INVERSE 3
#define USR_IVAL_FACE_UPPER 4
#define USR_ITP_UNIT_IN 8
#define USR_ITP_BUCKLING 10
#define USR_IVAL_BUCK_DEFAULT 999
#define USR_RVAL_BUCK_I -1
#define USR_RVAL_BUCK_SH -2
#define USR_ITP_SYMMETRY 11
#define USR_IVAL_SYMM_REFERENCE_1M -1
#define USR_IVAL_SYMM_DEPEND_ON_VALUES 0
#define USR_IVAL_SYMM_YES 1
#define USR_IVAL_SYMM_NO 2
#define USR_ITP_TRANS 13
#define USR_IVAL_TRANS_USER 0
#define USR_IVAL_TRANS_IYZ 1
#define USR_IVAL_TRANS_YS 2
#define USR_IVAL_TRANS_ZS 4
#define USR_IVAL_TRANS_APPLY 8
#define USR_ITP_SCL 14
#define USR_IVAL_SCL_NO_C2T 0
#define USR_IVAL_SCL_CLASS_1 1
#define USR_IVAL_SCL_CLASS_2 2
#define USR_IVAL_SCL_CLASS_3 3
#define USR_IVAL_SCL_CLASS_4 4
#define USR_ITP_LTAU 80
#define USR_ITP_LSIG 81
#define USR_ITP_POIS 82
#define USR_IVAL_POIS_NO 0
#define USR_IVAL_POIS_YES 1
#define USR_IVAL_POIS_FULL 2
#define USR_IVAL_POIS_ALL 3
#define USR_ITP_BEAM_X 100
#define USR_ITP_AK 101
#define USR_IVAL_SECTVAL_SPECIAL -2
#define USR_IVAL_SECTVAL_UNDFEFINED -1
#define USR_IVAL_SECTVAL_CALCULATED 0
#define USR_IVAL_SECTVAL_USER 1
#define USR_IVAL_SECTVAL_USER_SPECIAL 2
#define USR_ITP_AY 102
#define USR_ITP_AZ 103
#define USR_ITP_IT 104
#define USR_ITP_CM 105
#define USR_ITP_CMS 106
#define USR_ITP_AYZ 107
#define USR_ITP_YSC 108
#define USR_IVAL_SC_VALUE 1
#define USR_IVAL_SC_CENTER 2
#define USR_ITP_ZSC 109
#define USR_ITP_HYMIN 110
#define USR_ITP_HZMIN 111
#define USR_ITP_AKTZ 112
#define USR_ITP_DEFF 113
#define USR_ITP_B2TF 114
#define USR_ITP_B2TW 115
#define USR_ITP_AGAM 116
#define USR_ITP_NPLT 120
#define USR_ITP_NPLC 121
#define USR_ITP_VYPL 122
#define USR_ITP_VZPL 123
#define USR_ITP_MTPL 124
#define USR_ITP_MYPL 125
#define USR_ITP_MZPL 126
#define USR_ITP_MBPL 127
#define USR_ITP_MT2PL 128
#define USR_ITP_FIRST_CONTROL 201
#define USR_ITP_MIN_RF 211
#define USR_IVAL_RFCS_NO 0
#define USR_IVAL_RFCS_DEFAULT 1
#define USR_IVAL_RFCS_YES 2
#define USR_IVAL_RFCS_WEIGHT 3
#define USR_IVAL_RFCS_ALL 3
#define USR_IVAL_RFCS_EFECTIVE 4
#define USR_IVAL_RFCS_UNUSED_RF_MAT 8
#define USR_ITP_FIXL 212
#define USR_ITP_FACE_CTRL 213
#define USR_ITP_STYP 216
#define USR_IVAL_STYP_FORC 0
#define USR_IVAL_STYP_IT_ONLY 1
#define USR_IVAL_STYP_TORSION 2
#define USR_IVAL_STYP_FULL 3
#define USR_IVAL_STYP_BEM 4
#define USR_IVAL_STYP_BEM_1 5
#define USR_IVAL_STYP_BEM_2 6
#define USR_IVAL_STYP_BEM_3 7
#define USR_IVAL_STYP_FEM 11
#define USR_IVAL_STYP_FEMX 15
#define USR_ITP_SDIV 217
#define USR_IVAL_SDIV_NO 0
#define USR_IVAL_SDIV_2 1
#define USR_IVAL_SDIV_4 2
#define USR_IVAL_SDIV_8 3
#define USR_IVAL_SDIV_16 4
#define USR_IVAL_SDIV_32 5
#define USR_ITP_SINT 218
#define USR_IVAL_SINT_FEM_ELEM 1
#define USR_IVAL_SINT_FEM_NODE 2
#define USR_IVAL_SINT_FEM_LOBATTO 3
#define USR_IVAL_SINT_FEM_GAUSS 4
#define USR_IVAL_SINT_BEM_SHORT 1
#define USR_IVAL_SINT_BEM_DOUBLE 2
#define USR_IVAL_SINT_BEM_EQUATION 4
#define USR_IVAL_SINT_BEM_SYMMETRY 8
#define USR_IVAL_SINT_BEM_LOGPIT 256
#define USR_ITP_SHEAR_CUT 219
#define USR_IVAL_SCUT_NO 0
#define USR_IVAL_SCUT_MAIN 1
#define USR_IVAL_SCUT_BOTH 2
#define USR_IVAL_SCUT_FORC 8
#define USR_IVAL_SCUT_CORNER 16
#define USR_IVAL_SCUT_AXIS 32
#define USR_ITP_HMIN 220
#define USR_ITP_HDIV 221
#define USR_ITP_HTOL 222
#define USR_ITP_PLAS 226
#define USR_IVAL_PLAS_TABLE 0
#define USR_IVAL_PLAS_LINE 1
#define USR_IVAL_PLAS_NONL 2
#define USR_IVAL_PLAS_AREA 16
#define USR_ITP_GDIV 227
#define USR_ITP_FORCES 229
#define USR_IVAL_SAFETY_FORCES -999
#define USR_IVAL_SAFETY_VALUES -998
#define USR_IVAL_FORC_DEFAULT 0
#define USR_IVAL_FORC_TENSION 1
#define USR_IVAL_FORC_COMPRESSION 2
#define USR_IVAL_FORC_BUCKLING 8
#define USR_IVAL_FORC_GAMMA_M0 32
#define USR_IVAL_FORC_GAMMA_SCM 64
#define USR_ITP_MAX_COOR 230
#define USR_ITP_MIN_COOR 231
#define USR_ITP_LCTEMP 233
#define USR_ITP_FIRST_CS 293
#define USR_ITP_LAST_CS 356
#define USR_ITP_FIRST_MAT 357
#define USR_ITP_LAST_MAT 420
int     m_ival;                  /*        value of control flag */
float   m_rval;                  /*        real value of control flag */
float   m_rvald;                 /*        place to save the original value which has been */
} typeCDB_SECT_USR;

#define SECT_MAT_ID 91
#define SECT_MAT_VER 201507
typedef struct tagCDB_SECT_MAT { /* 9/NR  SectionalMaterial list */
int     m_id;                    /*        Identification = 91 */
int     m_mat[31];               /*        material numbers */
#define MAX_SECT_MAT 31
} typeCDB_SECT_MAT;

#define SECT_VAD_ID 97
#define SECT_VAD_VER 201310
typedef struct tagCDB_SECT_VAD { /* 9/NR  SectionVarDefaults */
int     m_id;                    /*        Identification = 97/98 */
int     m_ind1;                  /*        Lower Index of variable */
int     m_ind2;                  /*        Upper Index of variable */
int     m_idim;                  /*        Unit of variable */
chr     m_name[4];               /*        Name of variable */
float   m_rval[1020];            /*        values of the variable NAME(I1:I2) */
} typeCDB_SECT_VAD;

#define SECT_VAR_ID 98
#define SECT_VAR_VER 201310
typedef struct tagCDB_SECT_VAR { /* 9/NR  SectionVarValues */
int     m_id;                    /*        Identification = 97/98 */
int     m_ind1;                  /*        Lower Index of variable */
int     m_ind2;                  /*        Upper Index of variable */
int     m_idim;                  /*        Unit of variable */
chr     m_name[4];               /*        Name of variable */
float   m_rval[1020];            /*        values of the variable NAME(I1:I2) */
} typeCDB_SECT_VAR;

#define SECT_REF_ID 99
#define SECT_REF_VER 200501
typedef struct tagCDB_SECT_REF { /* 9/NR  SectionReference */
int     m_id;                    /*        Identification 099 */
int     m_im;                    /*        Type of reference structure definition (see AQUA.ERR) */
#define STEEL_SHAPE_BLOCK_BEGIN 100
#define REF_STRUCT_FIRST_COORD 101
#define REF_STRUCT_SECOND_COORD 102
#define REF_STRUCT_THIRD_COORD 103
#define REF_STRUCT_ANGLE 104
#define REF_STRUCT_THREE_SCALAR 105
#define REFERENCE_BLOCK_END 199
int     m_irts;                  /*        Subtype of reference definition for the points: */
#define REF_IRTS_ABSOLUTE_COORD 0
#define REF_IRTS_RELATIVE_NOSCALE 1
#define REF_IRTS_RELATIVE_SCALEDIST 2
#define REF_IRTS_RELATIVE_SCALEALL 3
#define REF_IRTS_POLAR 4
#define REF_IRTS_ABSOLUTE_Y 5
#define REF_IRTS_ABSOLUTE_Z 6
#define REF_IRTS_ABSOLUTE_P 7
#define REF_IRTS_ALL 7
#define REF_IRTS_ABSOLUTE_DIR 16
#define REF_IRTS_DIR_REF2 32
#define REF_IRTS_AXIS_REF 64
#define REF_IRTS_AXIS_REF_1 128
#define REF_IRTS_AXIS_REF_2 256
#define REF_IRTS_AXIS_REF_3 512
#define REF_IRTS_REFPOINT_1 1024
#define REF_IRTS_REFPOINT_2 131072
#define REF_IRTS_REFPOINT_3 16777216
chr     m_ref[200];              /*        Identifier of references (0 = not defined) */
} typeCDB_SECT_REF;

#define SECT_SPT_ID 100
#define SECT_SPT_VER 200805
typedef struct tagCDB_SECT_SPT { /* 9/NR  SectionStressPoint (SPT/QSP) */
int     m_id;                    /*        Identification 100/101 */
int     m_mno;                   /*        Material number */
int     m_idp;                   /*        Number*256 + Bitpattern */
#define SPT_IDP_IN 1
#define SPT_IDP_C2T 2
#define SPT_IDP_NEFF_Y 4
#define SPT_IDP_NEFF_Z 8
#define SPT_IDP_NEFF_N 16
#define SPT_IDP_FEM 32
#define SPT_IDP_GENERATED 64
#define SPT_IDP_FIBRE 128
#define SPT_IDP_NUMBER 256
#define PPT_IDP_IN 1
#define PPT_IDP_NEFF_EXPL 2
#define PPT_IDP_NEFF_Y 4
#define PPT_IDP_NEFF_Z 8
#define PPT_IDP_NEFF_N 16
#define PPT_IDP_TP 32
#define PPT_IDP_GENERATED 64
#define PPT_IDP_LAST 128
#define PPT_IDP_POLYGON 256
#define PPT_IDP_GENERATED_POLYGON 100
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate */
float   m_z;                     /* [1011] z-ordinate */
float   m_rt;                    /* [1011] Radius of arc   (:101) ; effect. thickness (:100) */
float   m_wty;                   /* [1018] Resistance Torsion */
float   m_wvyy;                  /* [1017] Resistance y-shear */
float   m_wvzy;                  /* [1017] Resistance z-shear */
float   m_wt2y;                  /* [1018] Resistance 2nd Torsion */
float   m_wtz;                   /* [1018] Resistance Torsion */
float   m_wvyz;                  /* [1017] Resistance y-shear */
float   m_wvzz;                  /* [1017] Resistance z-shear */
float   m_wt2z;                  /* [1018] Resistance 2nd Torsion */
float   m_w0;                    /* [1012] unit warping */
float   m_epsr;                  /*        non effective strain ratio (0 < epsr <= 1) */
float   m_exp;                   /*        property set */
float   m_fix;                   /* [1011] location of fix point */
float   m_sigd;                  /* [1092] max. dynamic allowed stress range sig-d(-1) */
float   m_taud;                  /* [1092] max dynamic allowed stress range tau-d(-1) */
chr     m_litd;                  /*        literal for notch type */
float   m_sigy;                  /* [1092] stress perpendicular */
float   m_aref;                  /* [1012] weight (effective area) FE-integration points */
float   m_temp;                  /* [1215] Temperature */
float   m_temp1;                 /* [1215] maximum temperature in history (for damage) */
} typeCDB_SECT_SPT;

#define SECT_PPT_ID 101
#define SECT_PPT_VER 200910
typedef struct tagCDB_SECT_PPT { /* 9/NR  SectionPolygonPoint (POLY/QPOL) */
int     m_id;                    /*        Identification 100/101 */
int     m_mno;                   /*        Material number */
int     m_idp;                   /*        Number*256 + Bitpattern */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate */
float   m_z;                     /* [1011] z-ordinate */
float   m_rt;                    /* [1011] Radius of arc   (:101) ; effect. thickness (:100) */
float   m_wty;                   /* [1018] Resistance Torsion */
float   m_wvyy;                  /* [1017] Resistance y-shear */
float   m_wvzy;                  /* [1017] Resistance z-shear */
float   m_wt2y;                  /* [1018] Resistance 2nd Torsion */
float   m_wtz;                   /* [1018] Resistance Torsion */
float   m_wvyz;                  /* [1017] Resistance y-shear */
float   m_wvzz;                  /* [1017] Resistance z-shear */
float   m_wt2z;                  /* [1018] Resistance 2nd Torsion */
float   m_w0;                    /* [1012] unit warping */
float   m_epsr;                  /*        non effective strain ratio (0 < epsr <= 1) */
float   m_exp;                   /*        property set */
float   m_fix;                   /* [1011] location of fix point */
float   m_sigd;                  /* [1092] max. dynamic allowed stress range sig-d(-1) */
float   m_taud;                  /* [1092] max dynamic allowed stress range tau-d(-1) */
chr     m_litd;                  /*        literal for notch type */
float   m_sigy;                  /* [1092] stress perpendicular */
float   m_aref;                  /* [1012] weight (effective area) FE-integration points */
float   m_temp;                  /* [1215] Temperature */
float   m_temp1;                 /* [1215] maximum temperature in history (for damage) */
} typeCDB_SECT_PPT;

#define SECT_CIR_ID 102
#define SECT_CIR_VER 200501
typedef struct tagCDB_SECT_CIR { /* 9/NR  SectionCircle (CIRC/KREI) */
int     m_id;                    /*        Identification 102 */
int     m_mno;                   /*        Material number */
int     m_itp;                   /*        interior construction */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate center */
float   m_z;                     /* [1011] z-ordinate center */
float   m_r;                     /* [1011] Radius */
float   m_ri;                    /* [1011] Radius hole */
float   m_uc;                    /*        property set */
} typeCDB_SECT_CIR;

#define SECT_PAN_ID 103
#define SECT_PAN_VER 201803
typedef struct tagCDB_SECT_PAN { /* 9/NR  SectionPanel (PLAT,WALL) */
int     m_id;                    /*        Identification 103/104 */
int     m_mno;                   /*        Material number */
int     m_idp;                   /*        Bit-pattern */
#define PAN_IDP_FIXA 8
#define PAN_IDP_FIXE 16
#define PAN_IDP_NEFF_EXPL 32
#define PAN_IDP_GENERATED 64
#define PAN_IDP_NOCHECK 128
#define PAN_IDP_SPLIT_NEFFA 256
#define PAN_IDP_SPLIT_NEFFE 512
#define PAN_IDP_NEFF_Y 1024
#define PAN_IDP_NEFF_Z 2048
#define PAN_IDP_NEFF_N 4096
#define PAN_IDP_SECTION 8192
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_t;                     /* [1010] thickness */
float   m_wa;                    /* [1012] unit warping start */
float   m_we;                    /* [1012] unit warping end */
float   m_wt;                    /* [1018] maximum shear from MT */
float   m_wvy;                   /* [1017] maximum shear from Vy */
float   m_wvz;                   /* [1017] maximum shear from Vz */
float   m_wt2;                   /* [1018] maximum shear from MT2 */
float   m_xvy;                   /*        relative Ordinate WVy */
float   m_xvz;                   /*        relative Ordinate WVz */
float   m_xmt2;                  /*        relative Ordinate Wt2 */
float   m_wtbr;                  /*        Bredt torsional shear */
float   m_fixa;                  /* [1011] location of fix point */
float   m_fixb;                  /* [1011] location of fix point */
float   m_teff;                  /* [1010] effective thickness */
float   m_ast;                   /* [1011] Distance of stiffeners along beam axis */
float   m_eta;                   /*        Reduction factor for local buckling */
} typeCDB_SECT_PAN;

#define SECT_WEL_ID 104
#define SECT_WEL_VER 201803
typedef struct tagCDB_SECT_WEL { /* 9/NR  SectionWeld  (WELD) */
int     m_id;                    /*        Identification 103/104 */
int     m_mno;                   /*        Material number */
int     m_idp;                   /*        Bit-pattern */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_t;                     /* [1010] thickness */
float   m_wa;                    /* [1012] unit warping start */
float   m_we;                    /* [1012] unit warping end */
float   m_wt;                    /* [1018] maximum shear from MT */
float   m_wvy;                   /* [1017] maximum shear from Vy */
float   m_wvz;                   /* [1017] maximum shear from Vz */
float   m_wt2;                   /* [1018] maximum shear from MT2 */
float   m_xvy;                   /*        relative Ordinate WVy */
float   m_xvz;                   /*        relative Ordinate WVz */
float   m_xmt2;                  /*        relative Ordinate Wt2 */
float   m_wtbr;                  /*        Bredt torsional shear */
float   m_fixa;                  /* [1011] location of fix point */
float   m_fixb;                  /* [1011] location of fix point */
float   m_teff;                  /* [1010] effective thickness */
float   m_ast;                   /* [1011] Distance of stiffeners along beam axis */
float   m_eta;                   /*        Reduction factor for local buckling */
} typeCDB_SECT_WEL;

#define SECT_PER_ID 110
#define SECT_PER_VER 200708
typedef struct tagCDB_SECT_PER { /* 9/NR  SectionPeriphery */
int     m_id;                    /*        Identification 110 */
int     m_mno;                   /*        Material number */
int     m_mn2;                   /*        2nd material number */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate */
float   m_za;                    /* [1011] z-ordinate */
float   m_ye;                    /* [1011] y-ordinate */
float   m_ze;                    /* [1011] z-ordinate */
float   m_air;                   /*        air contact ratio */
} typeCDB_SECT_PER;

#define SECT_FEM_ID 112
#define SECT_FEM_VER 201301
typedef struct tagCDB_SECT_FEM { /* 9/NR  SectionTopology */
int     m_id;                    /*        Identification 111 */
int     m_mno;                   /*        Material number */
int     m_nrel;                  /*        internal element number */
int     m_node[4];               /*        internal element nodes */
} typeCDB_SECT_FEM;

#define SECT_NER_ID 190
#define SECT_NER_VER 201511
typedef struct tagCDB_SECT_NER { /* 9/NR  SectionNonEffectiveRectangle (NEFF) */
int     m_id;                    /*        Identification 190 */
int     m_mno;                   /*        Material number */
int     m_idp;                   /*        Noneffective for (same bits as :101) */
#define NER_IDP_STRESS 0
#define NER_IDP_NEFF_Y 1
#define NER_IDP_NEFF_Z 2
#define NER_IDP_NEFF_N 4
#define NER_IDP_NOINVERT 8
#define NER_IDP_NEFF_R 16
#define NER_IDP_NEFF_R_USE 32
#define NER_IDP_GENERATED 64
#define NER_IDP_YMIN 2048
#define NER_IDP_ZMIN 4096
#define NER_IDP_YMAX 8192
#define NER_IDP_ZMAX 16384
chr     m_tnr;                   /*        Designation */
float   m_ymin;                  /* [1011] y-ordinate */
float   m_zmin;                  /* [1011] z-ordinate */
float   m_ymax;                  /* [1011] y-ordinate */
float   m_zmax;                  /* [1011] z-ordinate */
float   m_t;                     /* [1011] thickness */
float   m_smin;                  /* [1092] stress at point (ymin,zmin) */
float   m_smax;                  /* [1092] stress at point (ymax,zmax) */
} typeCDB_SECT_NER;

#define SECT_NEP_ID 191
#define SECT_NEP_VER 200810
typedef struct tagCDB_SECT_NEP { /* 9/NR  Partial non effective polygons */
int     m_id;                    /*        Identification 191 */
int     m_mno;                   /*        Materialnumber */
int     m_idp;                   /*        Noneffective for (see :190) */
chr     m_tnr;                   /*        Ident from 190 / Cut identifier for :312 elements */
float   m_yz[511][2];            /* [1011] yz(2,*) = vertices (old format is blocked with 7 points */
} typeCDB_SECT_NEP;

#define SECT_PRF_ID 200
#define SECT_PRF_VER 200501
typedef struct tagCDB_SECT_PRF { /* 9/NR  SectionPointReinforcement (RF/BEW) */
int     m_id;                    /*        Identification 200 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
#define PRF_IR_BITS 10
#define PRF_IR_NEFF_Y 1
#define PRF_IR_NEFF_Z 2
#define PRF_IR_NEFF_N 4
#define PRF_IR_NEFF_R 16
#define PRF_IR_OUTSIDE 128
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate */
float   m_z;                     /* [1011] z-ordinate */
float   m_sig;                   /* [1092] prestress */
float   m_temp;                  /* [1215] Temperature */
float   m_as;                    /* [1020] Reinforcement area */
float   m_asma;                  /* [1020] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
#define PRF_FT_NONE 0
#define PRF_FT_AKTI 1
#define PRF_FT_ADDI -1
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1012] Reference area */
float   m_a;                     /* [1011] distance of bars */
} typeCDB_SECT_PRF;

#define SECT_LRF_ID 201
#define SECT_LRF_VER 200501
typedef struct tagCDB_SECT_LRF { /* 9/NR  SectionLineReinforcement (LRF/LBEW) */
int     m_id;                    /*        Identification 201/211/221 or 321 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
#define LRP_IR_MEAS 10
#define LRP_IR_MEAS_LENGTH 0
#define LRP_IR_MEAS_TOTAL 1
#define LRP_IR_MEAS_ELEMENT 2
#define LRP_IR_DIST 100
#define LRP_IR_DIST_INS 0
#define LRP_IR_DIST_ADJA 1
#define LRP_IR_DIST_ADJE 2
#define LRP_IR_DIST_FULL 3
#define LRP_IR_DIST_EVEN 4
#define LRP_IR_DIST_INSC 5
#define LRP_IR_NUMP 1000
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_r;                     /* [1011] Radius */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_LRF;

#define SECT_LRP_ID 211
#define SECT_LRP_VER 201302
typedef struct tagCDB_SECT_LRP { /* 9/NR  SectionLineReinforcement in Points */
int     m_id;                    /*        Identification 201/211/221 or 321 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_r;                     /* [1011] Radius */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_LRP;

#define SECT_LSL_ID 221
#define SECT_LSL_VER 201112
typedef struct tagCDB_SECT_LSL { /* 9/NR  SectionShearLink */
int     m_id;                    /*        Identification 201/211/221 or 321 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_r;                     /* [1011] Radius */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_LSL;

#define SECT_CRF_ID 202
#define SECT_CRF_VER 200501
typedef struct tagCDB_SECT_CRF { /* 9/NR  SectionCircularReinforcement (CRF/KBEW) */
int     m_id;                    /*        Identification 202/212/222 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate center */
float   m_z;                     /* [1011] z-ordinate center */
float   m_r;                     /* [1011] Radius */
float   m_i_4;
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_CRF;

#define SECT_CRP_ID 212
#define SECT_CRP_VER 201210
typedef struct tagCDB_SECT_CRP { /* 9/NR  SectionCircularReinforcement in Points */
int     m_id;                    /*        Identification 202/212/222 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate center */
float   m_z;                     /* [1011] z-ordinate center */
float   m_r;                     /* [1011] Radius */
float   m_i_4;
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_CRP;

#define SECT_CSL_ID 222
#define SECT_CSL_VER 201112
typedef struct tagCDB_SECT_CSL { /* 9/NR  SectionHelicalShearLink */
int     m_id;                    /*        Identification 202/212/222 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate center */
float   m_z;                     /* [1011] z-ordinate center */
float   m_r;                     /* [1011] Radius */
float   m_i_4;
float   m_as;                    /* [1029] Reinforc. area */
float   m_asma;                  /* [1029] max Reinforcement area */
float   m_rng;                   /*        Layer number */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Diameter */
float   m_aref;                  /* [1019] Reference area = width of element */
float   m_a;                     /* [1011] distance of bars */
float   m_phia;                  /* [   5] start angle for generated points */
float   m_phie;                  /* [   5] end   angle for generated points */
} typeCDB_SECT_CSL;

#define SECT_URF_ID 210
#define SECT_URF_VER 201302
typedef struct tagCDB_SECT_URF { /* 9/NR  SectionPeriphericReinforcement (CURF,UBEW) */
int     m_id;                    /*        Identification 210/220 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_cover;                 /* [1024] static cover */
chr     m_exp[2];                /*        Exposition class */
float   m_wc;                    /*        weight for centering reinforcement */
float   m_as;                    /* [1021] Reinforc. area/length */
float   m_asma;                  /* [1021] max Reinforcement area */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Bar Diameter */
float   m_aref;                  /* [1019] Reference area for crack width */
float   m_a;                     /* [1011] distance of bars */
} typeCDB_SECT_URF;

#define SECT_USL_ID 220
#define SECT_USL_VER 201302
typedef struct tagCDB_SECT_USL { /* 9/NR  SectionPeriphericShearLink */
int     m_id;                    /*        Identification 210/220 */
int     m_mno;                   /*        Material number */
int     m_ir;                    /*        Layer number */
chr     m_tnr;                   /*        Designation */
float   m_cover;                 /* [1024] static cover */
chr     m_exp[2];                /*        Exposition class */
float   m_wc;                    /*        weight for centering reinforcement */
float   m_as;                    /* [1021] Reinforc. area/length */
float   m_asma;                  /* [1021] max Reinforcement area */
float   m_ft;                    /*        torsional effect */
float   m_d;                     /* [1023] Bar Diameter */
float   m_aref;                  /* [1019] Reference area for crack width */
float   m_a;                     /* [1011] distance of bars */
} typeCDB_SECT_USL;

#define SECT_CPT_ID 300
#define SECT_CPT_VER 200501
typedef struct tagCDB_SECT_CPT { /* 9/NR  SectionCutStressPoint */
int     m_id;                    /*        Identification 300 */
int     m_mno;                   /*        Material of Reinforc. + 1000*Materialnumber */
int     m_bit;                   /*        Cut-Index*128 + Bitpattern */
chr     m_tnr;                   /*        Designation */
float   m_y;                     /* [1011] y-ordinate */
float   m_z;                     /* [1011] z-ordinate */
float   m_b0;                    /* [1011] width of cut */
float   m_wtm;                   /* [1018] Resistance Torsion */
float   m_wtr;                   /* [1018] Resistance max Torsion */
float   m_wvy;                   /* [1017] Resistance y-shear */
float   m_wvz;                   /* [1017] Resistance z-shear */
float   m_frs;                   /*        Factor for total reinf */
float   m_w0;                    /* [1012] unit warping */
float   m_sigy;                  /* [1092] stress perpendicular */
float   m_bb;                    /* [1011] total width of cut */
float   m_vyfk;                  /*        part of total y-shear */
float   m_vzfk;                  /*        part of total z-shear */
float   m_bqfk;                  /*        partial width factor */
float   m_byfk;                  /* [1011] shear design width for Vy */
float   m_bzfk;                  /* [1011] shear design width for Vz */
} typeCDB_SECT_CPT;

#define SECT_CUT_ID 301
#define SECT_CUT_VER 201803
typedef struct tagCDB_SECT_CUT { /* 9/NR  ShearCut (CUT) */
int     m_id;                    /*        Identification 301 */
int     m_mno;                   /*        Material of Reinforc. + 1000*Materialnumber */
int     m_idp;                   /*        Bitpattern */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_b0;                    /* [1011] effective width */
float   m_n;                     /* [1111] Normal force perpend. */
float   m_m;                     /* [1114] bending moment perpend */
float   m_wtm;                   /* [1018] Resistance Torsion mid */
float   m_wtd;                   /* [1018] add tors.resist.edge */
float   m_vyfk;                  /*        part of total y-shear */
float   m_vzfk;                  /*        part of total z-shear */
float   m_bv;                    /* [1011] total effective width */
float   m_phib;                  /* [   5] inclination stirups */
float   m_irb;                   /*        number of shearlayer */
float   m_ty;                    /* [  17] unit shear Sy/Iz */
float   m_tz;                    /* [  17] unit shear Sz/Iy */
float   m_bred;                  /* [1011] reduction for ducts */
float   m_asbm;                  /*        Minimum shear reinf. */
float   m_betc;                  /*        friction coefficient */
float   m_mue;                   /*        friction in joint */
float   m_act;                   /* [1012] Area of cut off */
float   m_ksl;                   /*        parameters for plate */
float   m_sxe;                   /* [1011] crack spacing for AASTHO */
float   m_cott;                  /*        explicit selected inclination for truss model */
float   m_bmin;                  /* [1011] minimum of cut width */
float   m_bmax;                  /* [1011] maximum of cut width */
float   m_fakvy;                 /*        assumed total separated part for Vy */
float   m_fakvz;                 /*        assumed total separated part for Vz */
} typeCDB_SECT_CUT;

#define SECT_CPL_ID 311
#define SECT_CPL_VER 0
typedef struct tagCDB_SECT_CPL { /* 9/NR  Partial polygons of shear cut */
int     m_id;                    /*        Identification 311 */
int     m_mno;                   /*        Materialnumber */
int     m_idp;                   /*        Cut number (0=continue last one) */
chr     m_tnr;                   /*        Designation of cut */
float   m_yz[511][2];            /* [1011] yz(2,*) = up to seven vertices */
} typeCDB_SECT_CPL;

#define SECT_APL_ID 313
#define SECT_APL_VER 0
typedef struct tagCDB_SECT_APL { /* 9/NR  Partial polygons of AKT crack area */
int     m_id;                    /*        Identification 311 */
int     m_mno;                   /*        Materialnumber */
int     m_idp;                   /*        Cut number (0=continue last one) */
chr     m_tnr;                   /*        Designation of cut */
float   m_yz[511][2];            /* [1011] yz(2,*) = up to seven vertices */
} typeCDB_SECT_APL;

#define SECT_HYD_ID 900
#define SECT_HYD_VER 200501
typedef struct tagCDB_SECT_HYD { /* 9/NR  SectionalHydraulics total section */
int     m_id;                    /*        Identification = 900/903 */
int     m_i_1;
int     m_i_2;
float   m_a;                     /* [1012] Area */
float   m_ut;                    /* [1011] total periphery */
float   m_up;                    /* [1011] total wet periphery */
float   m_r;                     /* [1011] hydraulic radius */
float   m_zet1;                  /*        loss factor 1 */
float   m_zet2;                  /*        loss factor 2 */
float   m_slk;                   /*        total leakage */
float   m_ys;                    /* [1011] y-Ordinate Center */
float   m_zs;                    /* [1011] y-Ordinate Center */
} typeCDB_SECT_HYD;

#define SECT_HYP_ID 903
#define SECT_HYP_VER 200501
typedef struct tagCDB_SECT_HYP { /* 9/NR  SectionalHydraulics partial section */
int     m_id;                    /*        Identification = 900/903 */
int     m_i_1;
int     m_i_2;
float   m_a;                     /* [1012] Area */
float   m_ut;                    /* [1011] total periphery */
float   m_up;                    /* [1011] total wet periphery */
float   m_r;                     /* [1011] hydraulic radius */
float   m_zet1;                  /*        loss factor 1 */
float   m_zet2;                  /*        loss factor 2 */
float   m_slk;                   /*        total leakage */
float   m_ys;                    /* [1011] y-Ordinate Center */
float   m_zs;                    /* [1011] y-Ordinate Center */
} typeCDB_SECT_HYP;

#define SECT_HYA_ID 904
#define SECT_HYA_VER 200501
typedef struct tagCDB_SECT_HYA { /* 9/NR  SectionalHydraulicsAddval */
int     m_id;                    /*        identifier 904 */
int     m_i_2;
int     m_type;                  /*        type of section */
float   m_nw;                    /*        Nominal width */
float   m_ymin;                  /* [1011] Minimum Ordinate of section */
float   m_ymax;                  /* [1011] Maximum Ordinate of section */
float   m_zmin;                  /* [1011] Minimum Ordinate of section */
float   m_zmax;                  /* [1011] Maximum Ordinate of section */
float   m_ks;                    /* [  16] absolute roughness */
float   m_s;                     /*        storage coefficient */
float   m_i_8;                   /*        reserved */
float   m_i_9;                   /*        reserved */
float   m_i_10;                  /*        reserved */
float   m_hp;                    /* [1010] lift of pump */
float   m_lmin;                  /*        minimum power */
float   m_lmax;                  /*        maximum power */
float   m_qmax;                  /*        maximum delivery */
} typeCDB_SECT_HYA;

#define SECT_HYS_ID 910
#define SECT_HYS_VER 200501
typedef struct tagCDB_SECT_HYS { /* 9/NR  SectionalHydraulicsPolygon */
int     m_id;                    /*        Identification 910 */
int     m_mat;                   /*        identifier 0 (material number) */
int     m_nr;                    /*        number of side */
chr     m_tnr;                   /*        Designation */
float   m_ya;                    /* [1011] y-ordinate start */
float   m_za;                    /* [1011] z-ordinate start */
float   m_ye;                    /* [1011] y-ordinate end */
float   m_ze;                    /* [1011] z-ordinate end */
float   m_ks;                    /* [  16] absolute roughness */
float   m_slk;                   /*        leakage */
} typeCDB_SECT_HYS;

#define SECT_INS_ID 100002
#define SECT_INS_VER 0
typedef struct tagCDB_SECT_INS { /* 9/NR  structural database contents */
int     m_id;                    /*        Identification of record key */
int     m_kwh;                   /*        Identification of record key (30/31/32) */
int     m_kwl;                   /*        Identification of record key (=NR) */
int     m_ib[1];                 /*        buffer of record contents for record KWH/KWL */
                                 /*        unknown length! */
#define ANZOF_SECT_INS_IB(il) ((il)-offsetof(typeCDB_SECT_INS,m_ib))/sizeof(int)
} typeCDB_SECT_INS;


#define SECT_KWH 9
typedef union taguSECT {   /* 9/NR */
int m_id;
typeCDB_SECT m_sect;
typeCDB_SECT_EFF m_sect_eff;
typeCDB_SECT_PAR m_sect_par;
typeCDB_SECT_PEF m_sect_pef;
typeCDB_SECT_ERR m_sect_err;
typeCDB_SECT_ADD m_sect_add;
typeCDB_SECT_WAR m_sect_war;
typeCDB_SECT_PLA m_sect_pla;
typeCDB_SECT_DES m_sect_des;
typeCDB_SECT_PRE m_sect_pre;
typeCDB_SECT_GV m_sect_gv;
typeCDB_SECT_TRA m_sect_tra;
typeCDB_SECT_LAY m_sect_lay;
typeCDB_SECT_REC m_sect_rec;
typeCDB_SECT_ANN m_sect_ann;
typeCDB_SECT_TUB m_sect_tub;
typeCDB_SECT_CAB m_sect_cab;
typeCDB_SECT_PRO m_sect_pro;
typeCDB_SECT_TBA m_sect_tba;
typeCDB_SECT_IBA m_sect_iba;
typeCDB_SECT_TXT m_sect_txt;
typeCDB_SECT_WLS m_sect_wls;
typeCDB_SECT_WLU m_sect_wlu;
typeCDB_SECT_CW m_sect_cw;
typeCDB_SECT_WPA m_sect_wpa;
typeCDB_SECT_WDE m_sect_wde;
typeCDB_SECT_USR m_sect_usr;
typeCDB_SECT_MAT m_sect_mat;
typeCDB_SECT_VAD m_sect_vad;
typeCDB_SECT_VAR m_sect_var;
typeCDB_SECT_REF m_sect_ref;
typeCDB_SECT_SPT m_sect_spt;
typeCDB_SECT_PPT m_sect_ppt;
typeCDB_SECT_CIR m_sect_cir;
typeCDB_SECT_PAN m_sect_pan;
typeCDB_SECT_WEL m_sect_wel;
typeCDB_SECT_PER m_sect_per;
typeCDB_SECT_FEM m_sect_fem;
typeCDB_SECT_NER m_sect_ner;
typeCDB_SECT_NEP m_sect_nep;
typeCDB_SECT_PRF m_sect_prf;
typeCDB_SECT_LRF m_sect_lrf;
typeCDB_SECT_LRP m_sect_lrp;
typeCDB_SECT_LSL m_sect_lsl;
typeCDB_SECT_CRF m_sect_crf;
typeCDB_SECT_CRP m_sect_crp;
typeCDB_SECT_CSL m_sect_csl;
typeCDB_SECT_URF m_sect_urf;
typeCDB_SECT_USL m_sect_usl;
typeCDB_SECT_CPT m_sect_cpt;
typeCDB_SECT_CUT m_sect_cut;
typeCDB_SECT_CPL m_sect_cpl;
typeCDB_SECT_APL m_sect_apl;
typeCDB_SECT_HYD m_sect_hyd;
typeCDB_SECT_HYP m_sect_hyp;
typeCDB_SECT_HYA m_sect_hya;
typeCDB_SECT_HYS m_sect_hys;
typeCDB_SECT_INS m_sect_ins;
} typeuSECT;


/*   SOFiSTiK AG
     cdbtypesct.h
     end of automatically generated header
*/
