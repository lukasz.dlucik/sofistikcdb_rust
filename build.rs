extern crate bindgen;

use std::env;
use std::path::PathBuf;
// C:\Program Files\LLVM\bin
// C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\Llvm\x64\bin

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
   //println!("cargo:rustc-link-search=C:\\Users\\lukasz.dlucik\\Documents\\GIT\\sofistikAPI");
   // cargo:rustc-link-search=[KIND=]PATH 
    println!("cargo:rustc-link-lib=sof_cdb_w-70.dll");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("src/wrapper.h")
        .clang_arg("-v")
        .clang_arg("-IC:\\Users\\lukasz.dlucik\\Documents\\GIT\\sofistikAPI\\external_lib")
        .clang_arg("-x")
        .clang_arg("c++")
      //  .clang_arg(r"-IC:\Program Files\mingw-w64\x86_64-8.1.0-posix-seh-rt_v6-rev0\mingw64\lib\gcc\x86_64-w64-mingw32\8.1.0\include\c++")
        //.clang_arg(r"-target x86_64-pc-windows-gnu")
        
        
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}