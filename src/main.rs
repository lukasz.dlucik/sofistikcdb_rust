extern crate libc;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

fn main() {
    let filename = r"c:\aaa.cdb";

        // Convert Rust string to C
        let c_msg = match std::ffi::CString::new(r"c:\aaa.cdb") {
            Ok(s)  => s,
            Err(e) => return,
        };

 
 unsafe {
    println!("Taki test {}", FP_ILOGB0);
    let ie  = sof_cdb_init(c_msg.as_bytes().as_ptr() as *const i8, 99);
    println!("Taki wynik testu {}", ie);
}
}